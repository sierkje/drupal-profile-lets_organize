<?php

/**
 * @file
 * Defines the Let's Organize install screen by modifying the install form.
 *
 * Inspired by lightning.profile in drupal/lightning.
 * @see http://cgit.drupalcode.org/lightning/tree/lightning.profile?h=8.x-2.x
 */

use Drupal\Core\Link;
use Drupal\lets_organize\Form\ExtensionSelectForm;

/**
 * Implements hook_install_tasks().
 */
function lets_organize_install_tasks() {
  return [
    'lets_organize_select_extensions' => [
      'display_name' => t('Choose extensions'),
      'display' => TRUE,
      'type' => 'form',
      'function' => ExtensionSelectForm::class,
    ],
    'lets_organize_install_extensions' => [
      'display_name' => t('Install extensions'),
      'display' => TRUE,
      'type' => 'batch',
    ],
  ];
}

/**
 * Implements hook_install_tasks_alter().
 */
function lets_organize_install_tasks_alter(array &$tasks, array $install_tasks) {
  $tasks['install_finished']['function'] = 'lets_organize_post_install_redirect';
}

/**
 * Install task callback.
 *
 * Prepares batch job to install the enabled Let's Organize extensions.
 *
 * @param array $install_state
 *   The current install state.
 *
 * @return array
 *   The batch job definition.
 */
function lets_organize_install_extensions(array &$install_state) {
  $batch = [];
  /** @var array $modules */
  $modules = $install_state['lets_organize']['modules'];
  foreach ($modules as $module) {
    $batch['operations'][] = ['lets_organize_install_module', (array) $module];
  }

  return $batch;
}

/**
 * Batch API callback. Installs a module.
 *
 * @param string|array $module
 *   The name(s) of the module(s) to install.
 */
function lets_organize_install_module($module) {
  \Drupal::service('module_installer')
    ->install((array) $module);
}

/**
 * Redirects the user to a particular URL after installation.
 *
 * @param array $install_state
 *   The current install state.
 *
 * @return array
 *   A renderable array with a success message and a redirect header, if the
 *   extender is configured with one.
 */
function lets_organize_post_install_redirect(array &$install_state) {
  // The installer doesn't make it easy (possible?) to return a redirect
  // response, so set a redirection META tag in the output.
  $redirect_meta = [
    '#tag' => 'meta',
    '#attributes' => [
      'http-equiv' => 'refresh',
      'content' => '0;url=' . \Drupal::service('lets_organize.extender')
          ->getRedirect(),
    ],
  ];

  return [
    '#title' => t('Start organizing!'),
    'info' => [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('Your <em>@lets_organize</em> site is ready to go!', [
        '@lets_organize' => "Let's Organize",
      ]),
    ],
    'proceed_link' => [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('If you are not redirected in 5 seconds, @proceed_link.', [
        '@proceed_link' => Link::fromTextAndUrl(
          t('you can proceed to your site now'),
          \Drupal::service('lets_organize.extender')->getRedirect()
        ),
      ]),
    ],
    '#attached' => [
      'http_header' => [
        ['Cache-Control', 'no-cache'],
      ],
      'html_head' =>  [
        [$redirect_meta, 'meta_redirect'],
      ]
    ],
  ];
}
