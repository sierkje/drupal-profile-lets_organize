<?php

namespace Drupal\lets_organize\Form;

use Drupal\Core\Extension\ExtensionDiscovery;
use Drupal\Core\Extension\InfoParserInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\lets_organize\ProfileExtender;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form for selecting which Let's Organize extensions to install.
 *
 * Inspired by Drupal\lightning\Form\ExtensionSelectForm in drupal/lightning.
 * @see http://cgit.drupalcode.org/lightning/tree/src/Form/ExtensionSelectForm.php?h=8.x-2.x
 */
class ExtensionSelectForm extends FormBase {

  /**
   * The Let's Organize extender configuration object.
   *
   * @var \Drupal\lets_organize\ProfileExtender
   */
  protected $profileExtender;

  /**
   * The Drupal application root.
   *
   * @var string
   */
  protected $drupalRoot;

  /**
   * The info parser service.
   *
   * @var \Drupal\Core\Extension\InfoParserInterface
   */
  protected $infoParser;

  /**
   * The element info plugin manager.
   *
   * @var \Drupal\Core\Render\ElementInfoManagerInterface
   */
  protected $elementInfo;

  /**
   * Whether the form elements have been prepared.
   *
   * @var bool
   */
  protected $isFormPrepared = FALSE;

  /**
   * Form element 'actions'.
   *
   * @var array
   */
  protected $formElementActions = [];

  /**
   * Form element 'experimental'.
   *
   * @var array
   */
  protected $formElementExperimental = [];

  /**
   * Form element 'help'.
   *
   * @var array
   */
  protected $formElementHelp = [];

  /**
   * Form element 'modules'.
   *
   * @var array
   */
  protected $formElementModules = [];

  /**
   * Form element 'sub_components'.
   *
   * @var array
   */
  protected $formElementSubComponents = [];

  /**
   * Constructs a new ExtensionSelectForm.
   *
   * @param \Drupal\lets_organize\ProfileExtender $profile_extender
   *   The Let's Organize extender configuration object.
   * @param \SplString $drupal_root
   *   The Drupal application root.
   * @param \Drupal\Core\Extension\InfoParserInterface $info_parser
   *   The info parser service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Render\ElementInfoManagerInterface $element_info
   *   The element info plugin manager.
   */
  public function __construct(ProfileExtender $profile_extender, \SplString $drupal_root, InfoParserInterface $info_parser, TranslationInterface $string_translation, ElementInfoManagerInterface $element_info) {
    $this->profileExtender = $profile_extender;
    $this->drupalRoot = (string) $drupal_root;
    $this->infoParser = $info_parser;
    $this->stringTranslation = $string_translation;
    $this->elementInfo = $element_info;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   *   When a circular reference is detected.
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
   *   When the service is not defined.
   *
   * @see \Symfony\Component\DependencyInjection\ContainerInterface::get()
   *   For more info about any thrown exceptions.
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\lets_organize\ProfileExtender $profile_extender */
    $profile_extender = $container->get('lets_organize.profile_extender');
    /** @var \SplString $drupal_root */
    $drupal_root = $container->get('app.root');
    /** @var \Drupal\Core\Extension\InfoParserInterface $info_parser */
    $info_parser = $container->get('info_parser');
    /** @var \Drupal\Core\StringTranslation\TranslationInterface $string_translation */
    $string_translation = $container->get('string_translation');
    /** @var \Drupal\Core\Render\ElementInfoManagerInterface $element_info */
    $element_info = $container->get('element_info');


    return new static(
      $profile_extender,
      $drupal_root,
      $info_parser,
      $string_translation,
      $element_info
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lets_organize_extension_select';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#title'] = $this->t('Extensions');

    // Prepare and add the form elements.
    $this->prepareExtensionFormElements();
    $form['help'] = $this->formElementHelp;
    $form['modules'] = $this->formElementModules;
    // Only include the experimental section, if experimental extensions are
    // available.
    if (!empty($this->formElementExperimental)) {
      $form['experimental'] = $this->formElementExperimental;
    }
    $form['actions'] = $this->formElementActions;
    $form['sub_components'] = $this->formElementSubComponents;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var array $modules */
    $modules = $form_state->getValue('modules');
    /** @var array $experimental */
    $experimental = $form_state->getValue('experimental');
    /** @var array $sub_components */
    $sub_components = $form_state->getValue('sub_components');
    $yaml_excluded = $this->profileExtender->getExcludedComponents();
    $yaml_modules = $this->profileExtender->getModules();

    // Only install the experimental modules if they have explicitly accepted
    // the potential risks.
    if ($experimental['gate']) {
      $modules = array_merge($modules, $experimental['modules']);
    }
    $modules = array_filter($modules);

    // Merge in sub-components of enabled extensions.
    foreach ($modules as $module) {
      if (isset($sub_components[$module])) {
        $modules = array_merge($modules, $sub_components[$module]);
      }
    }

    // Remove the sub-components excluded by the extender.
    $modules = array_diff($modules, $yaml_excluded);

    // Merge the modules enabled by the extender.
    $modules = array_merge($modules, $yaml_modules);

    // Add the modules to the install state.
    $GLOBALS['install_state']['lets_organize']['modules'] = $modules;
  }

  /**
   * Process function to hide an element behind the experimental gate.
   *
   * @param array $element
   *   The element to process.
   *
   * @return array
   *   The processed element.
   */
  public static function addExperimentalGate(array $element) {
    // Hide the element if the experimental gate is not acknowledged.
    foreach (Element::children($element) as $key) {
      $element[$key]['#states']['visible']['#edit-experimental-gate']['checked'] = TRUE;
    }

    return $element;
  }

  /**
   * Forces the Let's Organize Core extension to be selected.
   *
   * Turns the Let's Organize Core checkbox into a persistent server-side value so
   * that it is always installed.
   *
   * @param array $element
   *   The set of checkboxes listing the available extensions.
   *
   * @return array
   *   The modified checkboxes.
   */
  public static function requireLetsOrganizeCoreModule(array $element) {
    return array_replace($element, [
      'lets_organize_core' => [
        '#type' => 'value',
        '#value' => $element['lets_organize_core']['#return_value'],
      ]
    ]);
  }

  /**
   * Returns the processing callbacks for form elements of type 'checkboxes'.
   *
   * @param array $additional
   *   Optional array of additional, custom process callback functions,
   *   taking $element, $form_state, and $complete_form.
   *
   * @return array
   *   An array of process callback functions for the 'checkboxes' element,
   *   including any defined standard and provided additional callbacks.
   */
  protected function getCheckboxesProcessingCallbacks(array $additional = []) {
    $checkboxes_info = $this->elementInfo->getInfo('checkboxes');

    return array_merge($checkboxes_info['#process'] ?? [],  $additional);
  }

  /**
   * Yields info for each of the Let's Organize extensions.
   *
   * @throws \Drupal\Core\Extension\InfoParserException
   *   Exception thrown if there is a parsing error or the .info.yml file does
   *   not contain a required key.
   *
   * @see \Drupal\Core\Extension\InfoParserInterface::parse()
   *   For more info about any thrown exceptions.
   */
  protected function getExtensionInfo() {
    $available = (new ExtensionDiscovery($this->drupalRoot))->scan('module');
    $default = $this->profileExtender->getDefaultLetsOrganizeExtensions();

    /** @var \Drupal\Core\Extension\Extension $extension */
    foreach (array_intersect_key($available, $default) as $key => $extension) {
      $info = $this->infoParser->parse($extension->getPathname());
      yield $key => $info;
    }
  }

  protected function prepareExtensionFormElements() {
    if (!$this->isFormPrepared) {
      $actions = [
        'continue' => [
          '#type' => 'submit',
          '#value' => $this->t('Continue'),
        ],
        '#type' => 'actions',
        '#weight' => 5,
      ];
      $experimental = [
        '#type' => 'fieldset',
        '#title' => $this->t('Experimental'),
        '#tree' => TRUE,
        '#weight' => 1,
        'gate' => [
          '#type' => 'checkbox',
          '#title' => $this->t('I understand the potential risks of experimental modules.'),
        ],
        'modules' => [
          '#type' => 'checkboxes',
          '#process' => $this->getCheckboxesProcessingCallbacks([
            [get_class($this), 'addExperimentalGate']
          ]),
        ],
      ];
      $help = [
        '#weight' => -1,
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $this->t("You can choose to disable some of the Let's Organize functionality below. However, it is not recommended."),
      ];
      $modules = [
        '#type' => 'checkboxes',
        '#weight' => 0,
        '#process' => $this->getCheckboxesProcessingCallbacks([
          [get_class($this), 'requireCore']
        ]),
      ];
      $sub_components = [
        '#type' => 'value',
        '#value' => [],
      ];

      // Add the available extensions to the 'modules' or 'experimental' element.
      foreach ($this->getExtensionInfo() as $key => $info) {
        if (empty($info['experimental'])) {
          $modules['#options'][$key] = $info['name'];
          $modules['#default_value'][] = $key;
        }
        else {
          $experimental['modules']['#options'][$key] = $info['name'];
        }

        // Store the list sub-components to avoid re-parsing the info file.
        if (isset($info['components'])) {
          $sub_components['#value'][$key] = $info['components'];
        }
      }

      // Do not include the experimental section, if there are no experimental
      // extensions available.
      if (!(boolean) $experimental['modules']['#options']) {
        $experimental = [];
      }

      // If the extender configuration has a pre-selected set of extensions, do
      // not allow the user to choose different ones.
      $preselected = $this->profileExtender->getLetsOrganizeExtensions();
      if (is_array($preselected)) {
        // Prevent selection of non-experimental extensions.
        $mods_default = array_keys($modules['#options']);
        $mods_default = array_intersect($mods_default, $preselected);
        $modules['#default_value'] = $mods_default;
        $modules['#disabled'] = TRUE;

        if (!empty($experimental)) {
          // Prevent selection of experimental extensions.
          $exp_default = $experimental['modules']['#options'];
          $exp_default = array_intersect($exp_default, $preselected);
          $experimental['modules']['#default_value'] = $exp_default;
          $experimental['modules']['#disabled'] = TRUE;

          // Acknowledge the experimental gate.
          $experimental['gate']['#disabled'] = TRUE;
          $experimental['gate']['#default_value'] = TRUE;
        }

        // Explain ourselves.
        $help['#markup'] = $this->t("Let's Organize extensions have been pre-selected in the lets_organize.extend.yml file in your sites directory and are disabled here as a result.");
      }

      // Set the form element definitions.
      $this->formElementActions = $actions;
      $this->formElementExperimental = $experimental;
      $this->formElementHelp = $help;
      $this->formElementModules = $modules;
      $this->formElementSubComponents = $sub_components;
    }
  }

}
