<?php

namespace Drupal\lets_organize;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Url;

/**
 * Helper class to get information from a site's lets_organize.extend.yml file.
 *
 * Inspired by Drupal\lightning\ProfileExtender in drupal/lightning.
 * @see http://cgit.drupalcode.org/lightning/tree/src/Extender.php?h=8.x-2.x
 */

class ProfileExtender {

  /**
   * The path to the Drupal root.
   *
   * @var string
   */
  protected $drupalRoot;

  /**
   * The path to the site's configuration (e.g. sites/default)
   *
   * @var string
   */
  protected $sitePath;

  protected $yaml;

  /**
   * ProfileExtender constructor.
   *
   * @param \SplString $drupal_root
   *   The path to the Drupal root.
   * @param \SplString $site_path
   *   The path to the site's configuration (e.g. sites/default).
   */
  public function __construct(\SplString $drupal_root, \SplString $site_path) {
    $this->drupalRoot = (string) $drupal_root;
    $this->sitePath = (string) $site_path;
    $this->yaml = [];

    // First discover lets_organize.extend.yml in the `sitePath`, and then defer
    // to `sites/all`.
    $paths[] = $this->sitePath . '/lets_organize.extend.yml';
    $paths[] = $this->drupalRoot . '/sites/all/lets_organize.extend.yml';

    foreach ($paths as $path) {
      if (file_exists($path)) {
        $yaml = file_get_contents($path);
        $this->yaml = array_merge($this->yaml, Yaml::decode($yaml));
      }
    }
  }

  /**
   * Returns the Let's Organize extensions that should be enabled.
   *
   * @return string[]|bool
   *   Either an array of extensions to enable, FALSE if all extensions should
   *   be enabled, or an empty array if no extensions should be enabled.
   */
  public function getLetsOrganizeExtensions() {
    // Return FALSE if the array of extensions is not available, because an
    // empty array means "don't enable _any_ extensions" in this case.
    $extensions = $this->yaml['lets_organize_extensions'] ?? FALSE;
    if (!empty($extensions)) {
      $extensions = $this->createKeyedArrayFromIndexedArray($extensions);
    }

    return $extensions ?? FALSE;
  }

  /**
   * Returns the Let's Organize sub-components that should NOT be installed.
   *
   * @return string[]
   *   The sub-components to exclude.
   */
  public function getExcludedComponents() {
    $components = $this->yaml['exclude_components'] ?? [];
    if (!empty($components)) {
      $components = $this->createKeyedArrayFromIndexedArray($components);
    }

    return $components;
  }

  /**
   * Returns the list of additional modules to enable.
   *
   * @return string[]
   *   The modules to enable.
   */
  public function getModules() {
    $modules = $this->yaml['modules'] ?? [];
    if (!empty($modules)) {
      $modules = $this->createKeyedArrayFromIndexedArray($modules);
    }

    return $modules;
  }

  /**
   * Returns the URL to redirect to after installation.
   *
   * @return string
   *
   * @throws \InvalidArgumentException
   *   When the redirect path in lets_organize.extend.yml is invalid.
   */
  public function getRedirect() {
    try {
      // Redirect to the front page by default.
      $path = '<front>';
      if (!empty($this->yaml['redirect']['path'])) {
        $path = ltrim($this->yaml['redirect']['path'], '/');
      }
      $redirect = Url::fromUri('internal:/' . $path);

      if (isset($this->yaml['redirect']['options'])) {
        $redirect->setOptions($this->yaml['redirect']['options']);
      }

      // Explicitly set the base URL, if not previously set, to prevent weird
      // redirection snafus.
      $base_url = $redirect->getOption('base_url');
      if (empty($base_url)) {
        $redirect->setOption('base_url', $GLOBALS['base_url']);
      }

      return $redirect->setOption('absolute', TRUE)->toString();
    }
    catch (\InvalidArgumentException $e) {
      // Somehow the redirect URL could not be created, this almost certainly
      // means that the redirect path in the .extend.yml file is not valid.
      $message = 'The redirect path in lets_organize.extend.yml is invalid.';

      throw new \InvalidArgumentException($message, $e->getCode(), $e);
    }
  }

  /**
   * Returns an array of default Let's Organize extensions.
   *
   * @return array
   *   A keyed array of extensions, using the machine name as key and value.
   */
  public function getDefaultLetsOrganizeExtensions() {
    return $this->createKeyedArrayFromIndexedArray([
      'lets_organize_core',
      'lets_organize_media',
      'lets_organize_node_article',
      'lets_organize_node_page',
    ]);
  }

  /**
   * Creates a keyed array from an indexed array, using values as keys.
   *
   * @param array $indexed_array
   *   An indexed array.
   *
   * @return array
   *   A keyed array, where each array key matches the array value.
   */
  protected function createKeyedArrayFromIndexedArray(array $indexed_array) {
    return array_combine($indexed_array, $indexed_array);
  }
}
